package com.borischistov.cg.test;

import com.borischistov.aseloader.FileLoader;
import com.borischistov.aseloader.models.GLCamera;
import com.borischistov.aseloader.models.GLLight;
import com.borischistov.aseloader.models.GLMaterial;
import com.borischistov.aseloader.models.GLObject;
import com.jogamp.opengl.util.GLBuffers;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLMatrixFunc;
import javax.media.opengl.glu.GLU;
import java.nio.FloatBuffer;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 21.10.13
 * Time: 10:53
 */
public class Renderer extends GLCanvas implements GLEventListener {

    private GLMaterial[] matArray;
    private Map<String, GLObject> objectMap;
    private Map<String, GLLight> lightMap;
    private Map<String, GLCamera> cameraMap;

    private GLCamera camera;
    private GLLight light;

    private int shadowMapTexture;

    private int[] viewPort = new int[4];

    private FloatBuffer lightProjection = GLBuffers.newDirectFloatBuffer(16);
    private FloatBuffer lightView = GLBuffers.newDirectFloatBuffer(16);
    private FloatBuffer cameraProjection = GLBuffers.newDirectFloatBuffer(16);
    private FloatBuffer cameraView = GLBuffers.newDirectFloatBuffer(16);
    private FloatBuffer biasMatrix = GLBuffers.newDirectFloatBuffer(new float[]{
            0.5f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.5f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.5f, 0.0f,
            0.5f, 0.5f, 0.5f, 1.0f});

    private float[] dimWhite = new float[]{0.2f, 0.2f, 0.2f};
    private float[] black = new float[]{0.0f, 0.0f, 0.0f};

    private static final int shadowMapSize = 2048;

    private GLU glu;

    public Renderer() {
        FileLoader loader = new FileLoader();
        loader.readFileASE(Paths.get("./model/testShadows.ASE"));

        matArray = loader.getMaterials();
        objectMap = loader.getObjects();
        lightMap = loader.getLights();
        cameraMap = loader.getCameras();
        camera = cameraMap.get("Camera001");
        light = lightMap.get("Omni001");

        this.addGLEventListener(this);
    }

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL2 gl = glAutoDrawable.getGL().getGL2();
        glu = new GLU();

        gl.glMatrixMode(gl.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glClearDepth(1.0f);
        gl.glDepthFunc(gl.GL_LEQUAL);

        gl.glBindTexture(gl.GL_TEXTURE_2D, shadowMapTexture);
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_DEPTH_COMPONENT, shadowMapSize, shadowMapSize, 0,
                gl.GL_DEPTH_COMPONENT, gl.GL_FLOAT, null);
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR);
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR);
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP);
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP);

        gl.glEnable(gl.GL_DEPTH_TEST);
        gl.glEnable(gl.GL_CULL_FACE);
        gl.glEnable(gl.GL_NORMALIZE);
        gl.glEnable(gl.GL_LIGHTING);
        gl.glEnable(gl.GL_LIGHT0);
        gl.glEnable(gl.GL_TEXTURE_2D);

        gl.glClearColor(0.5f, 0.6f, 0.7f, 1f);

        gl.glEnableClientState(gl.GL_VERTEX_ARRAY);
        gl.glEnableClientState(gl.GL_NORMAL_ARRAY);
        gl.glEnableClientState(gl.GL_TEXTURE_COORD_ARRAY);

        //Инициализация матриц
        gl.glPushMatrix();

        //Камера проекция
        gl.glLoadIdentity();
        glu.gluPerspective(camera.getFov(), 1.0f, camera.getHither(), camera.getYon());
        gl.glGetFloatv(GLMatrixFunc.GL_PROJECTION_MATRIX, cameraProjection);

        //Камера вид
        gl.glLoadIdentity();
        gl.glLoadMatrixf(GLBuffers.newDirectFloatBuffer(camera.getCm()));
        gl.glGetFloatv(GLMatrixFunc.GL_MODELVIEW_MATRIX, cameraView);

        //Источник света проекция
        gl.glLoadIdentity();
        glu.gluPerspective(camera.getFov(), 1f, 1, 1000f);
        gl.glGetFloatv(GLMatrixFunc.GL_PROJECTION_MATRIX, lightProjection);

        //Источник света вид
        gl.glLoadIdentity();
        glu.gluLookAt(light.getPosition()[0], light.getPosition()[1], light.getPosition()[2],
                camera.getTarget()[0], camera.getTarget()[1], camera.getTarget()[2],
                camera.getCm()[4], -camera.getCm()[5], camera.getCm()[6]);
        gl.glGetFloatv(GLMatrixFunc.GL_MODELVIEW_MATRIX, lightView);

        gl.glPopMatrix();

    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();

        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
        gl.glClear(GL.GL_DEPTH_BUFFER_BIT);

        gl.glGetIntegerv(GL.GL_VIEWPORT, viewPort, 0);

        //Первый проход Позиция источника света
        gl.glLoadIdentity();

        gl.glMatrixMode(gl.GL_PROJECTION);
        gl.glLoadMatrixf(lightProjection);

        gl.glMatrixMode(gl.GL_MODELVIEW);
        gl.glLoadMatrixf(lightView);

        //Устанавливаем размер вида равный размеру карты теней.
        gl.glViewport(0, 0, shadowMapSize, shadowMapSize);
        //Рисуем только заднии части
        gl.glCullFace(gl.GL_FRONT);

        gl.glShadeModel(gl.GL_FLAT);
        gl.glColorMask(false, false, false, false);

        drawObjects(gl, true);


        gl.glBindTexture(gl.GL_TEXTURE_2D, shadowMapTexture);
        gl.glCopyTexImage2D(GL2.GL_TEXTURE_2D, 0, GL2.GL_DEPTH_COMPONENT, 0, 0,
                shadowMapSize, shadowMapSize, 0);


        //Востанавливаем начальное состояние
        gl.glCullFace(gl.GL_BACK);
        gl.glShadeModel(gl.GL_SMOOTH);
        gl.glColorMask(true, true, true, true);

        //Второй проход для улучшения теней используем приглушонное освещение
        gl.glClear(GL.GL_DEPTH_BUFFER_BIT);

        gl.glViewport(viewPort[0], viewPort[1], viewPort[2], viewPort[3]);

        gl.glLoadIdentity();

        gl.glMatrixMode(gl.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glLoadMatrixf(cameraProjection);

        gl.glMatrixMode(gl.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glLoadMatrixf(camera.getCm(), 0);

        //Используем приглушонный свет для затененных мест
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, light.getPosition(), 0);
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_AMBIENT, dimWhite, 0);
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_DIFFUSE, dimWhite, 0);
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_SPECULAR, black, 0);


        gl.glPushMatrix();
        drawObjects(gl, true);
        gl.glPopMatrix();

        //Третий проход
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, light.getPosition(), 0);
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_DIFFUSE, light.getDiffuse(), 0);
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_SPECULAR, light.getSpecular(), 0);

        FloatBuffer textureMatrix = RenderHelper.multMatrix(biasMatrix, lightProjection);
        textureMatrix = RenderHelper.multMatrix(textureMatrix, lightView);
        /*
        System.out.println("LightProjection");
        printBuffer(lightProjection);
        System.out.println("LightView");
        printBuffer(lightView);
        System.out.println("Result");
        printBuffer(subResult);
        */
        //FloatBuffer textureMatrix = RenderHelper.multMatrix(biasMatrix, subResult);

        //Активизируем карту теней


        textureMatrix.position(0);
        gl.glTexGeni(gl.GL_S, gl.GL_TEXTURE_GEN_MODE, gl.GL_EYE_LINEAR);
        gl.glTexGenfv(gl.GL_S, gl.GL_EYE_PLANE, textureMatrix);
        gl.glEnable(gl.GL_TEXTURE_GEN_S);

        textureMatrix.position(4);
        gl.glTexGeni(gl.GL_T, gl.GL_TEXTURE_GEN_MODE, gl.GL_EYE_LINEAR);
        gl.glTexGenfv(gl.GL_T, gl.GL_EYE_PLANE, textureMatrix);
        gl.glEnable(gl.GL_TEXTURE_GEN_T);

        textureMatrix.position(8);
        gl.glTexGeni(gl.GL_R, gl.GL_TEXTURE_GEN_MODE, gl.GL_EYE_LINEAR);
        gl.glTexGenfv(gl.GL_R, gl.GL_EYE_PLANE, textureMatrix);
        gl.glEnable(gl.GL_TEXTURE_GEN_R);

        textureMatrix.position(12);
        gl.glTexGeni(gl.GL_Q, gl.GL_TEXTURE_GEN_MODE, gl.GL_EYE_LINEAR);
        gl.glTexGenfv(gl.GL_Q, gl.GL_EYE_PLANE, textureMatrix);
        gl.glEnable(gl.GL_TEXTURE_GEN_Q);


        gl.glBindTexture(gl.GL_TEXTURE_2D, shadowMapTexture);
        gl.glEnable(gl.GL_TEXTURE_2D);
        //Включаем сравнение
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_COMPARE_MODE, gl.GL_COMPARE_R_TO_TEXTURE);
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_COMPARE_FUNC, gl.GL_LEQUAL);
        gl.glTexParameteri(gl.GL_TEXTURE_2D, GL2.GL_DEPTH_TEXTURE_MODE, gl.GL_INTENSITY);
        gl.glAlphaFunc(gl.GL_GEQUAL, 0.99f);
        gl.glEnable(gl.GL_ALPHA_TEST);

        drawObjects(gl, true);

        //Disable textures and texgen
        gl.glDisable(gl.GL_TEXTURE_2D);

        gl.glDisable(gl.GL_TEXTURE_GEN_S);
        gl.glDisable(gl.GL_TEXTURE_GEN_T);
        gl.glDisable(gl.GL_TEXTURE_GEN_R);
        gl.glDisable(gl.GL_TEXTURE_GEN_Q);

        //Restore other states
        gl.glDisable(gl.GL_ALPHA_TEST);

        //reset matrices
        gl.glMatrixMode(gl.GL_PROJECTION);
        gl.glPopMatrix();
        gl.glMatrixMode(gl.GL_MODELVIEW);
        gl.glPopMatrix();

        gl.glFlush();
        gl.glFinish();
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        if (height == 0) height = 1;
        GL2 gl = drawable.getGL().getGL2();
        gl.glViewport(x, y, width, height);
        gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(camera.getFov(), (float) width / (float) height, camera.getHither(), camera.getYon());
        gl.glGetFloatv(GLMatrixFunc.GL_PROJECTION_MATRIX, cameraProjection);
        gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    private void drawObjects(GL2 gl, boolean shadowRender) {
        GLObject object;

        gl.glPushMatrix();
        object = objectMap.get("Box001");
        gl.glMultMatrixf(object.getTmFB());
        object.drawObject(gl, shadowRender);
        gl.glPopMatrix();

        gl.glPushMatrix();
        object = objectMap.get("Sphere001");
        gl.glMultMatrixf(object.getTmFB());
        object.drawObject(gl, shadowRender);
        gl.glPopMatrix();

        gl.glPushMatrix();
        object = objectMap.get("Box002");
        gl.glMultMatrixf(object.getTmFB());
        object.drawObject(gl, shadowRender);
        gl.glPopMatrix();
    }

    private void printBuffer(FloatBuffer buff) {
        System.out.print("[ ");
        for (int i = 0; i < buff.capacity(); i++) {
            if (i % 4 == 0 && i != 0) {
                System.out.println();
            }
            System.out.print(buff.get(i));
            if (i != buff.capacity() - 1) {
                System.out.print(", ");
            }
        }
        System.out.print(" ]");
        System.out.println();
    }
}

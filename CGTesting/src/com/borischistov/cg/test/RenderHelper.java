package com.borischistov.cg.test;

import com.jogamp.common.nio.Buffers;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import java.nio.FloatBuffer;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 07.01.14
 * Time: 21:53
 * Comment:
 */
public class RenderHelper {

    public static FloatBuffer multMatrix(FloatBuffer m1, FloatBuffer m2) {
        float[] result = new float[16];
        float[] mat1 = new float[16];
        float[] mat2 = new float[16];

        for (int i = 0; i < 16; i++) {
            mat1[i] = m1.get(i);
            mat2[i] = m2.get(i);
        }

        m1.rewind();
        m2.rewind();

        for (int i = 0; i < 16; i++) {
            int pos = i / 4;
            int col = i - pos * 4;
            result[i] = mat1[0 + pos * 4] * mat2[0 + col]
                    + mat1[1 + pos * 4] * mat2[4 + col]
                    + mat1[2 + pos * 4] * mat2[8 + col]
                    + mat1[3 + pos * 4] * mat2[12 + col];
        }
        return Buffers.newDirectFloatBuffer(result);
    }

    public static FloatBuffer createOrthoBuffer(
            float left, float right,
            float bottom, float top, float zNear, float zFar
    ) {
        float tx = -(right + left) / (right - left),
                ty = -(top + bottom) / (top - bottom),
                tz = -(zFar + zNear) / (zFar - zNear);

        return Buffers.newDirectFloatBuffer(new float[]{
                2 / (right - left), 0, 0, tx,
                0, 2 / (top - bottom), 0, ty,
                0, 0, -2 / (zFar - zNear), tz,
                0, 0, 0, 1
        });
    }

    public static int createTextureDepth(int width, int height, GL gl) {
        GL2 gl2 = gl.getGL2();

        //IntBuffer intBuffer = Buffers.newDirectIntBuffer();

        //gl2.glGenTextures(1, intBuffer);
        return 0;
    }
}

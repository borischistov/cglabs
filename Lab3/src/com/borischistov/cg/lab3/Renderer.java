package com.borischistov.cg.lab3;

import com.borischistov.aseloader.FileLoader;
import com.borischistov.aseloader.models.*;
import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLLightingFunc;
import javax.media.opengl.fixedfunc.GLMatrixFunc;
import javax.media.opengl.fixedfunc.GLPointerFunc;
import javax.media.opengl.glu.GLU;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 21.10.13
 * Time: 10:53
 */
public class Renderer extends GLCanvas implements GLEventListener {

    private GLMaterial[] matArray;
    private Map<String, GLObject> objectMap;
    private Map<String, GLLight> lightMap;
    private Map<String, GLCamera> cameraMap;

    float rotate = 0;
    float wheelRotate = 0f;
    float towerRotate = 45f;
    float towerSpeed = 0.3f;

    private ArrayList<Integer> textMap = new ArrayList<Integer>();

    private GLCamera camera;

    private float speed = 0.5f;

    private GLU glu;

    public Renderer() {
        FileLoader loader = new FileLoader();
        loader.readFileASE(Paths.get("./model/lab3.ase"));

        matArray = loader.getMaterials();
        objectMap = loader.getObjects();
        lightMap = loader.getLights();
        cameraMap = loader.getCameras();
        camera = cameraMap.get("Camera001");

        this.addGLEventListener(this);

    }

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL2 gl = glAutoDrawable.getGL().getGL2();
        glu = new GLU();

        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glEnable(GL.GL_CULL_FACE);
        gl.glEnable(GLLightingFunc.GL_NORMALIZE);
        gl.glEnable(GLLightingFunc.GL_LIGHTING);
        gl.glEnable(GLLightingFunc.GL_LIGHT0);
        gl.glEnable(GL.GL_TEXTURE_2D);

        gl.glClearColor(0.5f, 0.6f, 0.7f, 1f);

        gl.glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GLPointerFunc.GL_NORMAL_ARRAY);
        gl.glEnableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);

    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();


        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
        gl.glClear(GL.GL_DEPTH_BUFFER_BIT);

        gl.glColor3f(0.1f, 0.0f, 0.0f);

        gl.glLoadIdentity();

        GLLight light0 = lightMap.get("Omni001");
        gl.glLightfv(GLLightingFunc.GL_LIGHT0, GLLightingFunc.GL_DIFFUSE, Buffers.newDirectFloatBuffer(light0.getDiffuse()));
        gl.glLightfv(GLLightingFunc.GL_LIGHT0, GLLightingFunc.GL_AMBIENT, Buffers.newDirectFloatBuffer(light0.getAmbient()));
        gl.glLightfv(GLLightingFunc.GL_LIGHT0, GLLightingFunc.GL_SPECULAR, Buffers.newDirectFloatBuffer(light0.getSpecular()));
        gl.glLightfv(GLLightingFunc.GL_LIGHT0, GLLightingFunc.GL_POSITION, Buffers.newDirectFloatBuffer(light0.getPosition()));

        gl.glLoadMatrixf(Buffers.newDirectFloatBuffer(camera.getCm()));

        GLObject object = objectMap.get("BTRHull");
        gl.glRotatef(-rotate, 0f, 0f, 1f);
        gl.glMultMatrixf(object.getTmFB());
        object.drawObject(gl);

        gl.glPushMatrix();
        object = objectMap.get("WheelRF");
        gl.glMultMatrixf(object.getRtmFB());
        gl.glRotatef(20f, 0f, 1f, 0f);
        gl.glRotatef(wheelRotate, 0f, 0f, 1f);
        object.drawObject(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        object = objectMap.get("WheelRF2");
        gl.glMultMatrixf(object.getRtmFB());
        gl.glRotatef(20f, 0f, 1f, 0f);
        gl.glRotatef(wheelRotate, 0f, 0f, 1f);
        object.drawObject(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        object = objectMap.get("WheelRM");
        gl.glMultMatrixf(object.getRtmFB());
        gl.glRotatef(wheelRotate, 0f, 0f, 1f);
        object.drawObject(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        object = objectMap.get("WheelRB");
        gl.glMultMatrixf(object.getRtmFB());
        gl.glRotatef(wheelRotate, 0f, 0f, 1f);
        object.drawObject(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        object = objectMap.get("WheelLF");
        gl.glMultMatrixf(object.getRtmFB());
        gl.glRotatef(-20f, 0f, 1f, 0f);
        gl.glRotatef(wheelRotate, 0f, 0f, 1f);
        object.drawObject(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        object = objectMap.get("WheelLF2");
        gl.glMultMatrixf(object.getRtmFB());
        gl.glRotatef(-20f, 0f, 1f, 0f);
        gl.glRotatef(wheelRotate, 0f, 0f, 1f);
        object.drawObject(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        object = objectMap.get("WheelLM");
        gl.glMultMatrixf(object.getRtmFB());
        gl.glRotatef(wheelRotate, 0f, 0f, 1f);
        object.drawObject(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        object = objectMap.get("WheelLB");
        gl.glMultMatrixf(object.getRtmFB());
        gl.glRotatef(wheelRotate, 0f, 0f, 1f);
        object.drawObject(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        object = objectMap.get("Tower");
        gl.glMultMatrixf(object.getRtmFB());
        gl.glRotatef(towerRotate, 0f, 1f, 0f);
        object.drawObject(gl);
        object = objectMap.get("Weapon");
        gl.glMultMatrixf(object.getRtmFB());
        object.drawObject(gl);
        gl.glPopMatrix();

        gl.glFlush();

        if (rotate >= 360) rotate = 0;
        if (wheelRotate >= 360) wheelRotate = 0;
        rotate = rotate + 0.3f;
        wheelRotate = wheelRotate - 2f;
        if (towerRotate >= 45 || towerRotate <= -90) {
            towerSpeed = towerSpeed * -1;
        }
        towerRotate = towerRotate + towerSpeed;

    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        if (height == 0) height = 1;
        GL2 gl = drawable.getGL().getGL2();
        gl.glViewport(x, y, width, height);
        gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(camera.getFov(), (float) width / (float) height, camera.getHither(), camera.getYon());
        gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    private void LoadGLTexture(GL2 gl, GLTexture tex) {

        Texture texture = null;

        try {
            texture = TextureIO.newTexture(new File(tex.getTexture()), false);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        int pos = textMap.size();
        textMap.add(pos);

        gl.glBindTexture(GL.GL_TEXTURE_2D, pos);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glTexImage2D(
                GL.GL_TEXTURE_2D, 0, 3,
                texture.getWidth(), texture.getHeight(),
                0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, 0
        );
    }

    private void LoadGLTextures(GL2 gl, GLMaterial[] matArray) {
        for (int i = 0; i < matArray.length; i++) {
            GLTexture texture = matArray[i].getTexture();
            if (texture != null) {
                LoadGLTexture(gl, texture);
            } else if (matArray[i].getSubMats() != null) {
                LoadGLTextures(gl, matArray[i].getSubMats());
            }
        }
    }

}

package com.borischistov.cg.lab3;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 02.11.13
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class ObjectTree extends JPanel {

    public ObjectTree() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Loaded Objects");
        JTree tree = new JTree(root);
        JScrollPane treeView = new JScrollPane(tree);
        this.add(treeView);

    }
}

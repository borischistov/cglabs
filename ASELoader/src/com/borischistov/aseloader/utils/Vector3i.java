package com.borischistov.aseloader.utils;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 19.11.13
 * Time: 12:55
 */
public class Vector3i implements Vector<Integer> {

    private int x, y, z;

    public Vector3i() {
    }

    public Vector3i(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    @Override
    public Integer[] toArray() {
        return new Integer[]{
                x, y, z
        };
    }

    @Override
    public int size() {
        return 3;
    }


}

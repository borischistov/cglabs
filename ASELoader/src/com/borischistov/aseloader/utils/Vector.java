package com.borischistov.aseloader.utils;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 19.11.13
 * Time: 12:21
 */
public interface Vector<T> {

    public T[] toArray();

    public int size();

}

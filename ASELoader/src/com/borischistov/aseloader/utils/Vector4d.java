package com.borischistov.aseloader.utils;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 19.11.13
 * Time: 18:41
 */
public class Vector4d implements Vector<Double> {

    private double x,y,z,w;

    public Vector4d() {
    }

    public Vector4d(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public double getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public double getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    @Override
    public Double[] toArray() {
        return new Double[]{
                x,y,z,w
        };
    }

    @Override
    public int size() {
        return 4;
    }
}

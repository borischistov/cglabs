package com.borischistov.aseloader.utils;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 19.11.13
 * Time: 12:23
 */
public class Vector4f implements Vector<Float> {

    private float x, y, z, w;

    public Vector4f() {
    }

    public Vector4f(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    @Override
    public Float[] toArray() {
        return new Float[]{
            x, y, z, w
        };
    }

    @Override
    public int size() {
        return 4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector4f vector4f = (Vector4f) o;

        if (Float.compare(vector4f.w, w) != 0) return false;
        if (Float.compare(vector4f.x, x) != 0) return false;
        if (Float.compare(vector4f.y, y) != 0) return false;
        if (Float.compare(vector4f.z, z) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        result = 31 * result + (z != +0.0f ? Float.floatToIntBits(z) : 0);
        result = 31 * result + (w != +0.0f ? Float.floatToIntBits(w) : 0);
        return result;
    }
}

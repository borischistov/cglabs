package com.borischistov.aseloader.utils;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 19.11.13
 * Time: 18:40
 */
public class Vector3d implements Vector<Double> {
    private double x,y,z;

    public Vector3d() {
    }

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    @Override
    public Double[] toArray() {
        return new Double[]{
                x,y,z
        };
    }

    @Override
    public int size() {
        return 3;
    }
}

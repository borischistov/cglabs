package com.borischistov.aseloader.models;

import com.jogamp.common.nio.Buffers;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.fixedfunc.GLPointerFunc;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;

public class GLObject {

    private String name;

    private float[][] transformMatrix;
    private float[][] vertexes;
    private float[][] normals;
    private float[][] textureCoords;
    private float[][] rTransformMatrix;

    private FloatBuffer tmFB;
    private FloatBuffer vertexesFB;
    private FloatBuffer normalsFB;
    private FloatBuffer textureCoordsFB;
    private FloatBuffer rtmFB;
    private IntBuffer facesIB;

    private int[] faces;
    private int[] textureIndexes;

    private GLMaterial material;

    public GLObject(String name, float[][] transformMatrix, float[][] vertexes, int[] faces,
                    float[][] normals, float[][] textureCoords,
                    int[] textureIndexes, GLMaterial material, float[][] rTransformMatrix) {
        this.name = name;
        setTransformMatrix(transformMatrix);
        setVertexes(vertexes);
        setFaces(faces);
        setNormals(normals);
        setTextureCoords(textureCoords);
        this.textureIndexes = textureIndexes;
        this.material = material;
        setrTransformMatrix(rTransformMatrix);

    }

    public float[][] getTransformMatrix() {
        return transformMatrix;
    }

    public FloatBuffer getTmFB() {
        return tmFB;
    }

    public FloatBuffer getVertexesFB() {
        return vertexesFB;
    }

    public FloatBuffer getNormalsFB() {
        return normalsFB;
    }

    public FloatBuffer getTextureCoordsFB() {
        return textureCoordsFB;
    }

    public FloatBuffer getRtmFB() {
        return rtmFB;
    }

    public IntBuffer getFacesIB() {
        return facesIB;
    }

    public String getName() {
        return name;
    }

    public GLMaterial getMaterial() {
        return material;
    }

    public void setTransformMatrix(float[][] transformMatrix) {
        this.transformMatrix = transformMatrix;
        tmFB = getBuffer(transformMatrix);
    }

    public void setVertexes(float[][] vertexes) {
        this.vertexes = vertexes;
        vertexesFB = getBuffer(vertexes);
    }

    public void setNormals(float[][] normals) {
        this.normals = normals;
        normalsFB = getBuffer(normals);
    }

    public void setTextureCoords(float[][] textureCoords) {
        this.textureCoords = textureCoords;
        textureCoordsFB = getBuffer(textureCoords);
    }

    public void setrTransformMatrix(float[][] rTransformMatrix) {
        this.rTransformMatrix = rTransformMatrix;
        rtmFB = rTransformMatrix == null ? null : getBuffer(rTransformMatrix);
    }

    public void setFaces(int[] faces) {
        this.faces = faces;
        facesIB = Buffers.newDirectIntBuffer(faces);
    }

    private FloatBuffer getBuffer(float[][] array) {
        FloatBuffer buffer = Buffers.newDirectFloatBuffer(array.length * array[0].length);
        for (float[] subArray : array) {
            buffer.put(subArray);
        }
        buffer.rewind();
        return buffer;
    }

    public void drawObject(GL2 gl, boolean shadowRender) {
        if (shadowRender) {
            //gl.glDisable(GL2.GL_TEXTURE_2D);
            gl.glVertexPointer(3, GL.GL_FLOAT, 0, vertexesFB);
            gl.glNormalPointer(GL.GL_FLOAT, 0, normalsFB);
            //gl.glTexCoordPointer(2, GL.GL_FLOAT, 0, textureCoordsFB);
            gl.glColorMaterial(GL.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE & GL2.GL_SPECULAR);
            gl.glDisableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);
            gl.glDrawElements(GL.GL_TRIANGLES, facesIB.capacity(), GL.GL_UNSIGNED_INT, facesIB);
        } else {
            gl.glEnableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);
            gl.glEnable(GL2.GL_TEXTURE_2D);
            gl.glVertexPointer(3, GL.GL_FLOAT, 0, vertexesFB);
            gl.glNormalPointer(GL.GL_FLOAT, 0, normalsFB);
            gl.glTexCoordPointer(2, GL.GL_FLOAT, 0, textureCoordsFB);
            if (material != null) {
                if (material.getSubMats() != null) {
                    for (int i = 0; i < textureIndexes.length; i++) {
                        material.getSubMats()[textureIndexes[i]].setActive(gl);
                        gl.glDrawElements(GL.GL_TRIANGLES, 3, GL.GL_UNSIGNED_INT, facesIB.position(i * 3));
                        material.getSubMats()[textureIndexes[i]].setTextureDisable(gl);
                    }
                } else {
                    material.setActive(gl);
                    gl.glDrawElements(GL.GL_TRIANGLES, facesIB.capacity(), GL.GL_UNSIGNED_INT, facesIB);
                    material.setTextureDisable(gl);
                }
            }
            gl.glDisable(GL2.GL_TEXTURE_2D);
            gl.glDisableClientState(GLPointerFunc.GL_TEXTURE_COORD_ARRAY);

        }


    }

    public void drawObject(GL2 gl) {

        gl.glVertexPointer(3, GL.GL_FLOAT, 0, vertexesFB);
        gl.glNormalPointer(GL.GL_FLOAT, 0, normalsFB);
        gl.glTexCoordPointer(2, GL.GL_FLOAT, 0, textureCoordsFB);
        if (material != null) {
            if (material.getSubMats() != null) {
                for (int i = 0; i < textureIndexes.length; i++) {
                    material.getSubMats()[textureIndexes[i]].setActive(gl);
                    gl.glDrawElements(GL.GL_TRIANGLES, 3, GL.GL_UNSIGNED_INT, facesIB.position(i * 3));
                    material.getSubMats()[textureIndexes[i]].setTextureDisable(gl);
                }
            } else {
                material.setActive(gl);
                gl.glDrawElements(GL.GL_TRIANGLES, facesIB.capacity(), GL.GL_UNSIGNED_INT, facesIB);
                material.setTextureDisable(gl);
            }
        }

    }

    @Override
    public String toString() {
        StringBuilder vertexesSB = new StringBuilder();
        for (int i = 0; i < vertexes.length; i++) {
            for (int j = 0; j < 3; j++) {
                vertexesSB.append(vertexes[i][j] + " ");
            }
            vertexesSB.append("\n");
        }

        StringBuilder textureSB = new StringBuilder();
        for (int i = 0; i < textureCoords.length; i++) {
            for (int j = 0; j < 2; j++) {
                textureSB.append(textureCoords[i][j] + " ");
            }
            textureSB.append(" Current Position : " + i + "\n");
        }

        StringBuilder tmSB = new StringBuilder();
        for (int i = 0; i < transformMatrix.length; i++) {
            for (int j = 0; j < 4; j++) {
                tmSB.append(transformMatrix[i][j] + " ");
            }
            tmSB.append("\n");
        }

        StringBuilder rtmSB = new StringBuilder();
        if (rTransformMatrix != null) {
            for (int i = 0; i < rTransformMatrix.length; i++) {
                for (int j = 0; j < 4; j++) {
                    rtmSB.append(rTransformMatrix[i][j] + " ");
                }
                rtmSB.append("\n");
            }
        }

        return "GLObject{" +
                "name='" + name + '\'' +
                ", faces=" + Arrays.toString(faces) +
                ", textureIndexes=" + Arrays.toString(textureIndexes) +
                ", material=" + material +
                ", material=" + textureSB.toString() +
                '}';
    }
}

package com.borischistov.aseloader.models;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 20.11.13
 * Time: 13:00
 */
public class GLTexture {

    private int id;
    private float uOffset;
    private float vOffset;
    private float uTiling;
    private float vTiling;
    private String texture;

    public GLTexture() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getuOffset() {
        return uOffset;
    }

    public void setuOffset(float uOffset) {
        this.uOffset = uOffset;
    }

    public float getvOffset() {
        return vOffset;
    }

    public void setvOffset(float vOffset) {
        this.vOffset = vOffset;
    }

    public float getuTiling() {
        return uTiling;
    }

    public void setuTiling(float uTiling) {
        this.uTiling = uTiling;
    }

    public float getvTiling() {
        return vTiling;
    }

    public void setvTiling(float vTiling) {
        this.vTiling = vTiling;
    }

    public String getTexture() {
        return texture;
    }

    public void setTexture(String texture) {
        this.texture = texture;
    }
}

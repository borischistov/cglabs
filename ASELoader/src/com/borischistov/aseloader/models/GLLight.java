package com.borischistov.aseloader.models;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 12.11.13
 * Time: 15:41
 */
public class GLLight {

    private String name;

    private float[] diffuse;
    private float[] specular;
    private float[] ambient;
    private float[] position;
    private float range;
    private float[] spot_direction;
    private float spot_exponent;
    private float spot_cutoff;
    private float spot_beam;
    private float constant_attenuation;
    private float linear_attenuation;
    private float quadratic_attenuation;

    public GLLight(float[] diffuse, float[] specular, float[] ambient, float[] position,
                   float range, float[] spot_direction, float spot_exponent, float spot_cutoff,
                   float spot_beam, float constant_attenuation, float linear_attenuation,
                   float quadratic_attenuation, String name) {
        this.diffuse = diffuse;
        this.specular = specular;
        this.ambient = ambient;
        this.position = position;
        this.range = range;
        this.spot_direction = spot_direction;
        this.spot_exponent = spot_exponent;
        this.spot_cutoff = spot_cutoff;
        this.spot_beam = spot_beam;
        this.constant_attenuation = constant_attenuation;
        this.linear_attenuation = linear_attenuation;
        this.quadratic_attenuation = quadratic_attenuation;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float[] getDiffuse() {
        return diffuse;
    }

    public void setDiffuse(float[] diffuse) {
        this.diffuse = diffuse;
    }

    public float[] getSpecular() {
        return specular;
    }

    public void setSpecular(float[] specular) {
        this.specular = specular;
    }

    public float[] getAmbient() {
        return ambient;
    }

    public void setAmbient(float[] ambient) {
        this.ambient = ambient;
    }

    public float[] getPosition() {
        return position;
    }

    public void setPosition(float[] position) {
        this.position = position;
    }

    public float getRange() {
        return range;
    }

    public void setRange(float range) {
        this.range = range;
    }

    public float[] getSpot_direction() {
        return spot_direction;
    }

    public void setSpot_direction(float[] spot_direction) {
        this.spot_direction = spot_direction;
    }

    public float getSpot_exponent() {
        return spot_exponent;
    }

    public void setSpot_exponent(float spot_exponent) {
        this.spot_exponent = spot_exponent;
    }

    public float getSpot_cutoff() {
        return spot_cutoff;
    }

    public void setSpot_cutoff(float spot_cutoff) {
        this.spot_cutoff = spot_cutoff;
    }

    public float getSpot_beam() {
        return spot_beam;
    }

    public void setSpot_beam(float spot_beam) {
        this.spot_beam = spot_beam;
    }

    public float getConstant_attenuation() {
        return constant_attenuation;
    }

    public void setConstant_attenuation(float constant_attenuation) {
        this.constant_attenuation = constant_attenuation;
    }

    public float getLinear_attenuation() {
        return linear_attenuation;
    }

    public void setLinear_attenuation(float linear_attenuation) {
        this.linear_attenuation = linear_attenuation;
    }

    public float getQuadratic_attenuation() {
        return quadratic_attenuation;
    }

    public void setQuadratic_attenuation(float quadratic_attenuation) {
        this.quadratic_attenuation = quadratic_attenuation;
    }

    @Override
    public String toString() {
        return "GLLight{" +
                "name='" + name + '\'' +
                ", diffuse=" + Arrays.toString(diffuse) +
                ", specular=" + Arrays.toString(specular) +
                ", ambient=" + Arrays.toString(ambient) +
                ", position=" + Arrays.toString(position) +
                ", range=" + range +
                ", spot_direction=" + Arrays.toString(spot_direction) +
                ", spot_exponent=" + spot_exponent +
                ", spot_cutoff=" + spot_cutoff +
                ", spot_beam=" + spot_beam +
                ", constant_attenuation=" + constant_attenuation +
                ", linear_attenuation=" + linear_attenuation +
                ", quadratic_attenuation=" + quadratic_attenuation +
                '}';
    }
}

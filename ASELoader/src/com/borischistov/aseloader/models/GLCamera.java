package com.borischistov.aseloader.models;


import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 02.11.13
 * Time: 1:32
 * To change this template use File | Settings | File Templates.
 */
public class GLCamera {

    private String name;

    private float[] cm;
    private float[] target;
    private float[] position;
    private float hither;
    private float yon;
    private float fov;

    public GLCamera(float[] cm, float[] target, float[] position, float hither, float yon, float fov, String name) {
        this.cm = cm;
        this.hither = hither;
        this.yon = yon;
        this.fov = fov;
        this.name = name;
        this.target = target;
        this.position = position;
    }

    public float[] getCm() {
        return cm;
    }

    public void setCm(float[] cm) {
        this.cm = cm;
    }

    public float getHither() {
        return hither;
    }

    public void setHither(float hither) {
        this.hither = hither;
    }

    public float getYon() {
        return yon;
    }

    public void setYon(float yon) {
        this.yon = yon;
    }

    public float getFov() {
        return fov;
    }

    public void setFov(float fov) {
        this.fov = fov;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float[] getTarget() {
        return target;
    }

    public void setTarget(float[] target) {
        this.target = target;
    }

    public float[] getPosition() {
        return position;
    }

    public void setPosition(float[] position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "GLCamera{" +
                "name='" + name + '\'' +
                ", cm=" + Arrays.toString(cm) +
                ", hither=" + hither +
                ", yon=" + yon +
                ", fov=" + fov +
                '}';
    }
}

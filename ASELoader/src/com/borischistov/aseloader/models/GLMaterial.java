package com.borischistov.aseloader.models;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL2GL3;
import javax.media.opengl.fixedfunc.GLLightingFunc;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 12.11.13
 * Time: 14:52
 */
public class GLMaterial {

    private float[] diffuse;
    private float[] specular;
    private float[] ambient;
    private float[] selfIllum;
    private float shininess;
    private boolean twoSided;
    private GLMaterial[] subMats;
    private int id;
    private String matName;
    private GLTexture texture;
    private Texture tex;

    public GLMaterial(float[] diffuse, float[] specular, float[] ambient, float[] selfIllum,
                      float shininess, boolean twoSided, GLMaterial[] subMats, int id, String matName, GLTexture texture) {
        this.diffuse = diffuse;
        this.specular = specular;
        this.ambient = ambient;
        this.selfIllum = selfIllum;
        this.shininess = shininess;
        this.twoSided = twoSided;
        this.subMats = subMats;
        this.id = id;
        this.matName = matName;
        this.texture = texture;
    }

    public float[] getDiffuse() {
        return diffuse;
    }

    public void setDiffuse(float[] diffuse) {
        this.diffuse = diffuse;
    }

    public float[] getSpecular() {
        return specular;
    }

    public void setSpecular(float[] specular) {
        this.specular = specular;
    }

    public float[] getAmbient() {
        return ambient;
    }

    public void setAmbient(float[] ambient) {
        this.ambient = ambient;
    }

    public float[] getSelfIllum() {
        return selfIllum;
    }

    public void setSelfIllum(float[] selfIllum) {
        this.selfIllum = selfIllum;
    }

    public float getShininess() {
        return shininess;
    }

    public void setShininess(float shininess) {
        this.shininess = shininess;
    }

    public boolean isTwoSided() {
        return twoSided;
    }

    public void setTwoSided(boolean twoSided) {
        this.twoSided = twoSided;
    }

    public GLMaterial[] getSubMats() {
        return subMats;
    }

    public void setSubMats(GLMaterial[] subMats) {
        this.subMats = subMats;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMatName() {
        return matName;
    }

    public void setMatName(String matName) {
        this.matName = matName;
    }

    public GLTexture getTexture() {
        return texture;
    }

    public void setTexture(GLTexture texture) {
        this.texture = texture;
    }

    public void setActive(GL2 gl) {
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GLLightingFunc.GL_AMBIENT, Buffers.newDirectFloatBuffer(ambient));
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GLLightingFunc.GL_DIFFUSE, Buffers.newDirectFloatBuffer(diffuse));
        gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GLLightingFunc.GL_SPECULAR, Buffers.newDirectFloatBuffer(specular));
        gl.glMaterialf(GL.GL_FRONT_AND_BACK, GLLightingFunc.GL_SHININESS, shininess);
        if (texture != null) {
            try {
                if (tex == null) {
                    Path path = Paths.get(texture.getTexture());
                    String fileName = path.getFileName().toString();
                    tex = TextureIO.newTexture(new File("./model/Textures/" + fileName), false);
                }
                tex.setTexParameteri(gl, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
                tex.setTexParameteri(gl, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
                tex.setTexParameteri(gl, GL.GL_TEXTURE_WRAP_S, GL2GL3.GL_REPEAT);
                tex.setTexParameteri(gl, GL.GL_TEXTURE_WRAP_T, GL2GL3.GL_REPEAT);

                tex.enable(gl);
                tex.bind(gl);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setTextureDisable(GL2 gl) {
        if (tex != null) tex.disable(gl);
    }

    @Override
    public String toString() {
        return "\n" + "GLMaterial{" +
                "diffuse=" + Arrays.toString(diffuse) +
                ", specular=" + Arrays.toString(specular) +
                ", ambient=" + Arrays.toString(ambient) +
                ", selfIllum=" + Arrays.toString(selfIllum) +
                ", shininess=" + shininess +
                ", twoSided=" + twoSided +
                ", subMats=" + Arrays.toString(subMats) +
                ", id=" + id +
                ", matName='" + matName + '\'' +
                '}';
    }
}

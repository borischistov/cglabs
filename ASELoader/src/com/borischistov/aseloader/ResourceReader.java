package com.borischistov.aseloader;

import com.borischistov.aseloader.models.*;
import com.borischistov.aseloader.utils.Vector;
import com.borischistov.aseloader.utils.*;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.io.BufferedReader;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 13.11.13
 * Time: 11:58
 */
class ResourceReader {

    private GLMaterial[] matArray = null;
    private Map<String, GLObject> objectMap = new HashMap<String, GLObject>();
    private Map<String, GLLight> lightMap = new HashMap<String, GLLight>();
    private Map<String, GLCamera> cameraMap = new HashMap<String, GLCamera>();

    public ResourceReader() {
        Locale.setDefault(Locale.US);
    }

    public void parseFile(BufferedReader reader) {
        Scanner scanner = new Scanner(reader);

        boolean area = false;

        while (scanner.hasNext()) {
            Scanner lineScanner = new Scanner(scanner.nextLine());
            String param = lineScanner.next();
            if (param.equalsIgnoreCase("*MATERIAL_LIST")) {
                area = lineScanner.next().equalsIgnoreCase("{");
                while (area) {
                    lineScanner = new Scanner(scanner.nextLine());
                    param = lineScanner.next();
                    if (param.equalsIgnoreCase("*MATERIAL_COUNT")) {
                        int matCount = lineScanner.nextInt();
                        matArray = new GLMaterial[matCount];
                        for (int i = 0; i < matArray.length; i++) {
                            matArray[i] = parseMaterial(scanner);
                        }
                    } else if (param.equalsIgnoreCase("}")) {
                        area = false;
                    }
                }
            } else if (param.equalsIgnoreCase("*GEOMOBJECT")) {
                if (lineScanner.next().equalsIgnoreCase("{")) {
                    GLObject object = parseObject(scanner);
                    objectMap.put(object.getName(), object);
                }
            } else if (param.equalsIgnoreCase("*LIGHTOBJECT")) {
                if (lineScanner.next().equalsIgnoreCase("{")) {
                    GLLight light = parseLight(scanner);
                    lightMap.put(light.getName(), light);
                }
            } else if (param.equalsIgnoreCase("*CAMERAOBJECT")) {
                if (lineScanner.next().equalsIgnoreCase("{")) {
                    GLCamera camera = parseCameras(scanner);
                    cameraMap.put(camera.getName(), camera);
                }
            }
        }
    }

    public GLCamera parseCameras(Scanner scanner) {
        boolean area = true;

        String name = null;

        float[] cm = new float[16];
        float[] target = new float[4];
        float[] position = new float[4];
        float hither = 0;
        float yon = 0;
        float fov = 0;
        boolean alreadyParsed = false;

        while (area) {
            Scanner lineScanner = new Scanner(scanner.nextLine());
            String param = lineScanner.next();

            if (param.equalsIgnoreCase("*NODE_NAME")) {
                lineScanner.useDelimiter("\\z");
                name = lineScanner.next().replaceAll("\"", "");
                name = name.replaceAll(" ", "");

            } else if (param.equalsIgnoreCase("*NODE_TM") && !alreadyParsed) {
                boolean subArea = lineScanner.next().equalsIgnoreCase("{");
                while (subArea) {
                    lineScanner = new Scanner(scanner.nextLine());
                    param = lineScanner.next();
                    if (param.equalsIgnoreCase("*TM_ROW0")) {
                        cm[0] = Float.valueOf(lineScanner.next());
                        cm[1] = Float.valueOf(lineScanner.next());
                        cm[2] = Float.valueOf(lineScanner.next());
                        cm[3] = 0.0f;
                    } else if (param.equalsIgnoreCase("*TM_ROW1")) {
                        cm[4] = Float.valueOf(lineScanner.next());
                        cm[5] = Float.valueOf(lineScanner.next());
                        cm[6] = Float.valueOf(lineScanner.next());
                        cm[7] = 0.0f;
                    } else if (param.equalsIgnoreCase("*TM_ROW2")) {
                        cm[8] = Float.valueOf(lineScanner.next());
                        cm[9] = Float.valueOf(lineScanner.next());
                        cm[10] = Float.valueOf(lineScanner.next());
                        cm[11] = 0.0f;
                    } else if (param.equalsIgnoreCase("*TM_ROW3")) {
                        cm[12] = Float.valueOf(lineScanner.next());
                        cm[13] = Float.valueOf(lineScanner.next());
                        cm[14] = Float.valueOf(lineScanner.next());
                        cm[15] = 1.0f;
                        position[0] = cm[12];
                        position[1] = cm[13];
                        position[2] = cm[14];
                        position[3] = cm[15];
                    } else if (param.equalsIgnoreCase("}")) {
                        subArea = false;
                        alreadyParsed = true;
                    }
                }
            } else if (param.equalsIgnoreCase("*NODE_TM")) {
                boolean subArea = lineScanner.next().equalsIgnoreCase("{");
                while (subArea) {
                    lineScanner = new Scanner(scanner.nextLine());
                    param = lineScanner.next();
                    if (param.equalsIgnoreCase("*TM_ROW3")) {
                        target[0] = Float.valueOf(lineScanner.next());
                        target[1] = Float.valueOf(lineScanner.next());
                        target[2] = Float.valueOf(lineScanner.next());
                        target[3] = 1.0f;
                    } else if (param.equalsIgnoreCase("}")) {
                        subArea = false;
                    }
                }
            } else if (param.equalsIgnoreCase("*CAMERA_SETTINGS")) {
                boolean subArea = lineScanner.next().equalsIgnoreCase("{");
                while (subArea) {
                    lineScanner = new Scanner(scanner.nextLine());
                    param = lineScanner.next();
                    if (param.equalsIgnoreCase("*CAMERA_HITHER")) {
                        hither = Float.valueOf(lineScanner.next());
                    } else if (param.equalsIgnoreCase("*CAMERA_YON")) {
                        yon = Float.valueOf(lineScanner.next());
                    } else if (param.equalsIgnoreCase("*CAMERA_FOV")) {
                        float rads = Float.valueOf(lineScanner.next());
                        fov = (float) Math.toDegrees(rads);
                    } else if (param.equalsIgnoreCase("}")) {
                        subArea = false;
                    }
                }
            } else if (param.equalsIgnoreCase("}")) {
                area = false;
            }
        }
        float[] cmNew = new float[cm.length];
        cmNew[0] = cm[0];
        cmNew[1] = cm[4];
        cmNew[2] = cm[8];
        cmNew[3] = cm[3];
        cmNew[4] = cm[1];
        cmNew[5] = cm[5];
        cmNew[6] = cm[9];
        cmNew[7] = cm[7];
        cmNew[8] = cm[2];
        cmNew[9] = cm[6];
        cmNew[10] = cm[10];
        cmNew[11] = cm[11];
        cmNew[12] = -(cm[12] * cmNew[0] + cm[13] * cmNew[4] + cm[14] * cmNew[8]);
        cmNew[13] = -(cm[12] * cmNew[1] + cm[13] * cmNew[5] + cm[14] * cmNew[9]);
        cmNew[14] = -(cm[12] * cmNew[2] + cm[13] * cmNew[6] + cm[14] * cmNew[10]);
        cmNew[15] = 1;
        return new GLCamera(cmNew, target, position, hither, yon, fov, name);
    }

    public GLLight parseLight(Scanner scanner) {
        boolean area = true;

        String name = null;

        float[] diffuse = new float[]{1.0000f, 1.0000f, 1.0000f, 1.0000f};
        float[] specular = new float[]{1.0000f, 1.0000f, 1.0000f, 1.0000f};
        float[] ambient = new float[]{0.0000f, 0.0000f, 0.0000f, 1.0000f};
        float[] position = null;
        float range = 0;
        float[] spot_direction = new float[]{0.0000f, 0.0000f, 1.0000f};
        float spot_exponent = 1;
        float spot_cutoff = 0;
        float spot_beam = 0;
        float constant_attenuation = 1;
        float linear_attenuation = 0;
        float quadratic_attenuation = 0;
        while (area) {
            Scanner lineScanner = new Scanner(scanner.nextLine());
            String param = lineScanner.next();
            if (param.equalsIgnoreCase("*NODE_NAME")) {
                lineScanner.useDelimiter("\\z");
                name = lineScanner.next().replaceAll("\"", "");
                name = name.replaceAll(" ", "");
            } else if (param.equalsIgnoreCase("*NODE_TM")) {
                boolean subArea = lineScanner.next().equalsIgnoreCase("{");
                while (subArea) {
                    lineScanner = new Scanner(scanner.nextLine());
                    param = lineScanner.next();
                    if (param.equalsIgnoreCase("*TM_POS")) {
                        position = new float[]{
                                Float.valueOf(lineScanner.next()),
                                Float.valueOf(lineScanner.next()),
                                Float.valueOf(lineScanner.next()),
                                1
                        };
                    } else if (param.equalsIgnoreCase("}")) {
                        subArea = false;
                    }
                }
            } else if (param.equalsIgnoreCase("*LIGHT_SETTINGS")) {
                boolean subArea = lineScanner.next().equalsIgnoreCase("{");
                while (subArea) {
                    lineScanner = new Scanner(scanner.nextLine());
                    param = lineScanner.next();
                    if (param.equalsIgnoreCase("*LIGHT_COLOR")) {

                    } else if (param.equalsIgnoreCase("}")) {
                        subArea = false;
                    }
                }
            } else if (param.equalsIgnoreCase("}")) {
                area = false;
            }
        }
        return new GLLight(
                diffuse, specular,
                ambient, position, range, spot_direction,
                spot_exponent, spot_cutoff, spot_beam, constant_attenuation,
                linear_attenuation, quadratic_attenuation, name
        );

    }

    public GLMaterial parseMaterial(Scanner scanner) {
        Scanner lineScanner = new Scanner(scanner.nextLine());
        String param = lineScanner.next();
        boolean matArea = false;
        if (param.equalsIgnoreCase("*MATERIAL") ||
                param.equalsIgnoreCase("*SUBMATERIAL")) {

            int matIndex = lineScanner.nextInt();
            String matName = null;
            float[] ambient = null;
            float[] diffuse = null;
            float[] specular = null;
            float[] selfIllum = null;
            float shininess = 0f;
            GLMaterial[] subMats = null;
            GLTexture texture = null;
            GLMaterial result = null;

            matArea = lineScanner.next().equals("{");
            while (matArea) {
                lineScanner = new Scanner(scanner.nextLine());
                param = lineScanner.next();
                if (param.equalsIgnoreCase("*MATERIAL_NAME")) {
                    lineScanner.useDelimiter("\\z");
                    matName = lineScanner.next().replaceAll("\"", "");
                    matName = matName.replaceAll(" ", "");
                } else if (param.equalsIgnoreCase("*MATERIAL_AMBIENT")) {
                    ambient = new float[]{
                            Float.valueOf(lineScanner.next()),
                            Float.valueOf(lineScanner.next()),
                            Float.valueOf(lineScanner.next()),
                            1
                    };
                } else if (param.equalsIgnoreCase("*MATERIAL_DIFFUSE")) {
                    diffuse = new float[]{
                            Float.valueOf(lineScanner.next()),
                            Float.valueOf(lineScanner.next()),
                            Float.valueOf(lineScanner.next()),
                            1
                    };
                } else if (param.equalsIgnoreCase("*MATERIAL_SPECULAR")) {
                    specular = new float[]{
                            Float.valueOf(lineScanner.next()),
                            Float.valueOf(lineScanner.next()),
                            Float.valueOf(lineScanner.next()),
                            1
                    };
                } else if (param.equalsIgnoreCase("*MATERIAL_SELFILLUM")) {
                    ArrayList<Float> array = new ArrayList<Float>();
                    while (lineScanner.hasNextFloat()) {
                        array.add(Float.valueOf(lineScanner.next()));
                    }
                    if (array.size() != 0) {
                        selfIllum = new float[array.size()];
                        for (int i = 0; i < selfIllum.length; i++) {
                            selfIllum[i] = array.get(i);
                        }
                    } else {
                        selfIllum = new float[]{0, 0, 0, 1f};
                    }
                } else if (param.equalsIgnoreCase("*MATERIAL_SHINE")) {
                    float val = Float.valueOf(lineScanner.next());
                    shininess = val * 128f;
                } else if (param.equalsIgnoreCase("*MATERIAL_SHINESTRENGTH")) {
                    float strength = Float.valueOf(lineScanner.next());
                    for (int i = 0; i < specular.length - 1; i++) {
                        specular[i] = specular[i] * strength;
                    }

                } else if (param.equalsIgnoreCase("*MAP_DIFFUSE")) {
                    boolean subArea = lineScanner.next().equalsIgnoreCase("{");
                    texture = new GLTexture();
                    while (subArea) {
                        lineScanner = new Scanner(scanner.nextLine());
                        param = lineScanner.next();
                        if (param.equalsIgnoreCase("*BITMAP")) {
                            lineScanner.useDelimiter("\\z");
                            String file = lineScanner.next().replaceAll("\"", "");
                            file = file.replaceFirst(" *", "");
                            texture.setTexture(file);
                        } else if (param.equalsIgnoreCase("*UVW_U_OFFSET")) {
                            texture.setuOffset(Float.valueOf(lineScanner.next()));
                        } else if (param.equalsIgnoreCase("*UVW_V_OFFSET")) {
                            texture.setvOffset(Float.valueOf(lineScanner.next()));
                        } else if (param.equalsIgnoreCase("*UVW_U_TILING")) {
                            texture.setuTiling(Float.valueOf(lineScanner.next()));
                        } else if (param.equalsIgnoreCase("*UVW_V_TILING")) {
                            texture.setvTiling(Float.valueOf(lineScanner.next()));
                        } else if (param.equalsIgnoreCase("}")) {
                            subArea = false;
                        }
                    }
                } else if (param.equalsIgnoreCase("*NUMSUBMTLS")) {
                    int count = lineScanner.nextInt();
                    subMats = new GLMaterial[count];
                    for (int i = 0; i < count; i++) {
                        subMats[i] = parseMaterial(scanner);
                    }
                } else if (param.equalsIgnoreCase("}")) {
                    result = new GLMaterial(diffuse, specular, ambient, selfIllum, shininess, false, subMats, matIndex, matName, texture);
                    matArea = false;
                }
            }
            return result;

        } else {
            return null;
        }
    }

    public GLObject parseObject(Scanner scanner) {

        boolean area = true;

        String name = null;
        String parentName = null;

        Vector<Float>[] tm = new Vector[4];
        int materialID = 0;

        ArrayList<Vector<Float>> vertexArray = new ArrayList<Vector<Float>>();
        ArrayList<Vector<Integer>> facesIndexes = new ArrayList<Vector<Integer>>();
        ArrayList<Integer> faceMaterialIDs = new ArrayList<Integer>();
        ArrayList<Vector<Float>> textureVertexArray = new ArrayList<Vector<Float>>();
        ArrayList<Vector<Float>> normalsArray = new ArrayList<Vector<Float>>();
        ArrayList<Vector<Integer>> textureFacesIndexes = new ArrayList<Vector<Integer>>();

        Scanner lineScanner;
        while (area) {
            lineScanner = new Scanner(scanner.nextLine());
            String param = lineScanner.next();
            if (param.equalsIgnoreCase("*NODE_NAME")) {
                lineScanner.useDelimiter("\\z");
                name = lineScanner.next().replaceAll("\"", "");
                name = name.replaceAll(" ", "");
            } else if (param.equalsIgnoreCase("*NODE_PARENT")) {
                lineScanner.useDelimiter("\\z");
                parentName = lineScanner.next().replaceAll("\"", "");
                parentName = parentName.replaceAll(" ", "");
            } else if (param.equalsIgnoreCase("*MATERIAL_REF")) {
                materialID = lineScanner.nextInt();
            } else if (param.equalsIgnoreCase("*NODE_TM")) {
                boolean subArea = lineScanner.next().equalsIgnoreCase("{");
                while (subArea) {
                    lineScanner = new Scanner(scanner.nextLine());
                    param = lineScanner.next();
                    if (param.equalsIgnoreCase("*TM_ROW0")) {
                        tm[0] = new Vector4f(
                                lineScanner.nextFloat(),
                                lineScanner.nextFloat(),
                                lineScanner.nextFloat(),
                                0f);
                    } else if (param.equalsIgnoreCase("*TM_ROW1")) {
                        tm[1] = new Vector4f(
                                lineScanner.nextFloat(),
                                lineScanner.nextFloat(),
                                lineScanner.nextFloat(),
                                0f);
                    } else if (param.equalsIgnoreCase("*TM_ROW2")) {
                        tm[2] = new Vector4f(
                                lineScanner.nextFloat(),
                                lineScanner.nextFloat(),
                                lineScanner.nextFloat(),
                                0f);
                    } else if (param.equalsIgnoreCase("*TM_ROW3")) {
                        tm[3] = new Vector4f(
                                lineScanner.nextFloat(),
                                lineScanner.nextFloat(),
                                lineScanner.nextFloat(),
                                1f);
                    } else if (param.equalsIgnoreCase("}")) {
                        subArea = false;
                    }
                }
            } else if (param.equalsIgnoreCase("*MESH")) {
                boolean subArea = lineScanner.next().equalsIgnoreCase("{");
                while (subArea) {
                    lineScanner = new Scanner(scanner.nextLine());
                    param = lineScanner.next();
                    if (param.equalsIgnoreCase("*MESH_VERTEX_LIST")) {
                        boolean subSubArea = lineScanner.next().equalsIgnoreCase("{");
                        while (subSubArea) {
                            lineScanner = new Scanner(scanner.nextLine());
                            param = lineScanner.next();
                            if (param.equalsIgnoreCase("*MESH_VERTEX")) {
                                int id = lineScanner.nextInt(); //??? Пока не знаю зачем мне нужен
                                Vector3f vector = new Vector3f(
                                        lineScanner.nextFloat(),
                                        lineScanner.nextFloat(),
                                        lineScanner.nextFloat()
                                );
                                vertexArray.add(vector);
                            } else if (param.equalsIgnoreCase("}")) {
                                subSubArea = false;
                            }
                        }
                    } else if (param.equalsIgnoreCase("*MESH_FACE_LIST")) {
                        boolean subSubArea = lineScanner.next().equalsIgnoreCase("{");
                        while (subSubArea) {
                            lineScanner = new Scanner(scanner.nextLine());
                            param = lineScanner.next();
                            if (param.equalsIgnoreCase("*MESH_FACE")) {
                                String val = lineScanner.next();
                                int index = Integer.parseInt(val.substring(0, val.length() - 1));    // Пока не нужно
                                int a = -1, b = -1, c = -1;
                                while (lineScanner.hasNext()) {
                                    param = lineScanner.next();
                                    if (param.equalsIgnoreCase("A:")) {
                                        a = lineScanner.nextInt();
                                    } else if (param.equalsIgnoreCase("B:")) {
                                        b = lineScanner.nextInt();
                                    } else if (param.equalsIgnoreCase("C:")) {
                                        c = lineScanner.nextInt();
                                    } else if (param.equalsIgnoreCase("*MESH_MTLID")) {
                                        faceMaterialIDs.add(lineScanner.nextInt());
                                    }
                                }
                                facesIndexes.add(new Vector3i(a, b, c));
                            } else if (param.equalsIgnoreCase("}")) {
                                subSubArea = false;
                            }
                        }
                    } else if (param.equalsIgnoreCase("*MESH_TVERTLIST")) {
                        boolean subSubArea = lineScanner.next().equalsIgnoreCase("{");
                        while (subSubArea) {
                            lineScanner = new Scanner(scanner.nextLine());
                            param = lineScanner.next();
                            if (param.equalsIgnoreCase("*MESH_TVERT")) {
                                lineScanner.next();
                                textureVertexArray.add(new Vector2f(
                                        lineScanner.nextFloat(),
                                        lineScanner.nextFloat()
                                ));
                            } else if (param.equalsIgnoreCase("}")) {
                                subSubArea = false;
                            }
                        }
                    } else if (param.equalsIgnoreCase("*MESH_NORMALS")) {
                        boolean subSubArea = lineScanner.next().equalsIgnoreCase("{");
                        int ind = 0;
                        while (subSubArea) {
                            lineScanner = new Scanner(scanner.nextLine());
                            param = lineScanner.next();
                            if (param.equalsIgnoreCase("*MESH_VERTEXNORMAL")) {
                                int index = lineScanner.nextInt();
                                normalsArray.add(new Vector3f(
                                        Float.valueOf(lineScanner.next()),
                                        Float.valueOf(lineScanner.next()),
                                        Float.valueOf(lineScanner.next())
                                ));
                            } else if (param.equalsIgnoreCase("}")) {
                                subSubArea = false;
                            }
                        }
                    } else if (param.equalsIgnoreCase("*MESH_TFACELIST")) {
                        boolean subSubArea = lineScanner.next().equalsIgnoreCase("{");
                        while (subSubArea) {
                            lineScanner = new Scanner(scanner.nextLine());
                            param = lineScanner.next();
                            if (param.equalsIgnoreCase("*MESH_TFACE")) {
                                int id = lineScanner.nextInt() * 3; // Пока не нужно
                                Vector3i vector3i = new Vector3i(
                                        lineScanner.nextInt(),
                                        lineScanner.nextInt(),
                                        lineScanner.nextInt()
                                );
                                textureFacesIndexes.add(vector3i);
                            } else if (param.equalsIgnoreCase("}")) {
                                subSubArea = false;
                            }
                        }
                    } else if (param.equalsIgnoreCase("}")) {
                        subArea = false;
                    }
                }
            } else if (param.equalsIgnoreCase("}")) {
                area = false;
            }
        }

        int vertexSize = vertexArray.get(0).size();
        int texturesSize = textureVertexArray.get(0).size();
        int normalSize = normalsArray.get(0).size();
        int facesSize = facesIndexes.get(0).size();

        float[][] transformMatrix = new float[tm.length][tm[0].size()];

        for (int i = 0; i < tm.length; i++) {
            Vector<Float> vector = tm[i];
            for (int j = 0; j < vector.size(); j++) {
                transformMatrix[i][j] = vector.toArray()[j];
            }
        }


        RealMatrix tMatrix = MatrixUtils.createRealMatrix(float2Double(transformMatrix));
        RealMatrix inverseTM = new LUDecomposition(tMatrix).getSolver().getInverse();
        GLObject parent = objectMap.get(parentName);

        float[][] rtm = null;

        if (parent != null) {
            float[][] pTransformMatrix = parent.getTransformMatrix();
            RealMatrix parentTMatrix = MatrixUtils.createRealMatrix(float2Double(pTransformMatrix));
            parentTMatrix = new LUDecomposition(parentTMatrix).getSolver().getInverse();
            RealMatrix matrix = tMatrix.multiply(parentTMatrix);
            rtm = double2Float(matrix.getData());
        }

        float[][] vertexes = new float[facesIndexes.size() * facesSize][vertexSize];
        float[][] textures = new float[facesIndexes.size() * facesSize][texturesSize];
        float[][] normals = new float[facesIndexes.size() * facesSize][normalSize];
        int[] facesIndex = new int[facesIndexes.size() * facesSize];
        int[] matIDS = new int[facesIndexes.size()];

        for (int i = 0; i < facesIndexes.size(); i++) {

            Vector<Integer> vertexIndex = facesIndexes.get(i);
            matIDS[i] = faceMaterialIDs.get(i);

            Vector<Integer> textureCoords = textureFacesIndexes.get(i);

            for (int j = 0; j < vertexIndex.size(); j++) {
                Vector<Float> vertex = vertexArray.get(vertexIndex.toArray()[j]);
                facesIndex[i * facesSize + j] = i * facesSize + j;

                Vector<Float> normal = normalsArray.get(i * facesSize + j);

                for (int k = 0; k < normal.size(); k++) {
                    normals[i * facesSize + j][k] = normal.toArray()[k];
                }

                for (int k = 0; k < vertex.size(); k++) {
                    vertexes[i * facesSize + j][k] = vertex.toArray()[k];
                }
                RealMatrix mt = MatrixUtils.createRowRealMatrix(new double[]{
                        vertexes[i * facesSize + j][0],
                        vertexes[i * facesSize + j][1],
                        vertexes[i * facesSize + j][2],
                        1
                });

                RealMatrix result = mt.multiply(inverseTM);
                double[] res = result.getRow(0);
                vertexes[i * facesSize + j] = new float[]{
                        (float) res[0],
                        (float) res[1],
                        (float) res[2]
                };
            }

            for (int j = 0; j < textureCoords.size(); j++) {

                Vector<Float> texture = textureVertexArray.get(textureCoords.toArray()[j]);
                for (int k = 0; k < texture.size(); k++) {
                    textures[i * facesSize + j][k] = texture.toArray()[k];
                }
            }

        }

        return new GLObject(name, transformMatrix, vertexes, facesIndex, normals, textures, matIDS,
                matArray[materialID], rtm);
    }

    public GLMaterial[] getMatArray() {
        return matArray;
    }

    public Map<String, GLObject> getObjectMap() {
        return objectMap;
    }

    public Map<String, GLLight> getLightMap() {
        return lightMap;
    }

    public Map<String, GLCamera> getCameraMap() {
        return cameraMap;
    }

    private float[] double2Float(double[] row) {
        float[] result = new float[row.length];
        for (int i = 0; i < row.length; i++) {
            result[i] = (float) row[i];
        }
        return result;
    }

    private double[] float2Double(float[] row) {
        double[] result = new double[row.length];
        for (int i = 0; i < row.length; i++) {
            result[i] = row[i];
        }
        return result;
    }

    private float[][] double2Float(double[][] array) {
        float[][] result = new float[array.length][];
        for (int i = 0; i < array.length; i++) {
            result[i] = new float[array[i].length];
            for (int j = 0; j < array[i].length; j++) {
                result[i][j] = (float) array[i][j];
            }
        }
        return result;
    }

    private double[][] float2Double(float[][] array) {
        double[][] result = new double[array.length][];
        for (int i = 0; i < array.length; i++) {
            result[i] = new double[array[i].length];
            for (int j = 0; j < array[i].length; j++) {
                result[i][j] = array[i][j];
            }
        }
        return result;
    }
}

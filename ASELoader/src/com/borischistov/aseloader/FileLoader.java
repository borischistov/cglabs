package com.borischistov.aseloader;

import com.borischistov.aseloader.models.GLCamera;
import com.borischistov.aseloader.models.GLLight;
import com.borischistov.aseloader.models.GLMaterial;
import com.borischistov.aseloader.models.GLObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 13.11.13
 * Time: 12:36
 */
public class FileLoader {

    private ResourceReader resourceReader;

    public static void main(String args[]){
        new FileLoader().readFileASE(Paths.get("c:/BoxMats.ASE"));
    }

    public void readFileASE(Path file) {
        resourceReader = new ResourceReader();
        try {
            resourceReader.parseFile(Files.newBufferedReader(file, Charset.defaultCharset()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<String, GLObject> getObjects() {
        return resourceReader.getObjectMap();
    }

    public Map<String, GLLight> getLights() {
        return resourceReader.getLightMap();
    }

    public Map<String, GLCamera> getCameras() {
        return resourceReader.getCameraMap();
    }

    public GLMaterial[] getMaterials() {
        return resourceReader.getMatArray();
    }
}

package com.borischistov.cg.lab2;

import com.jogamp.opengl.util.FPSAnimator;

import javax.media.opengl.awt.GLCanvas;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 21.10.13
 * Time: 10:34
 */
public class AppStarter extends JFrame {

    public AppStarter() {
        this.setTitle("CG Lab 2");
        this.setSize(600, 600);
        this.setLayout(new BorderLayout());
        GLCanvas canvas = new Renderer();
        //ObjectTree tree = new ObjectTree();
        final FPSAnimator animator = new FPSAnimator(canvas, 60, true);
        this.add(canvas, BorderLayout.CENTER);
        //this.add(tree, BorderLayout.WEST);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                animator.stop();
                System.exit(0);
            }
        });
        this.setVisible(true);
        animator.start();
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new AppStarter();
            }
        });
    }
}
